/*
 * @Author: 霍格沃兹测试开发学社-盖盖
 * @Desc: '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
 */
package com.ceshiren.basic;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Thread.sleep;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * 添加成员测试用例
 */
public class Member2Test extends BaseTest{


    /**
     * 在通讯录页面进行，添加成员测试用例
     */

    //1. 进入通讯录页面
    //2. 点击添加成员按钮
    //3. 填写成员信息
    //4. 点击添加
    //断言

    @Test
    @DisplayName("通讯录页面添加成员")
    public void addMemberWithConcatPage(){
        //元素定位
       //1. 点击通讯录 id="menu_contacts"
        //01- 元素定位「8大元素定位中的id定位」
        By menuContacts = By.id("menu_contacts");
        //02- 查找元素 返回元素对象
        WebElement menuContactsEle = webDriver.findElement(menuContacts);
        //03- 元素操作 点击
        menuContactsEle.click();

        //强制等待
        try {
            sleep(15000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        //显示等待
        /**
         * ......
         */

        //2. 点击添加成员按钮
        
        By addMember = By.cssSelector(".js_has_member .js_add_member");//查找到2个元素
        //list集合里面的下标为0的元素
        WebElement addMemberEle = webDriver.findElement(addMember);
        for (int i = 0; i < 3; i++) {
            if(!webDriver.getPageSource().contains("username")){
                addMemberEle.click();
                try {
                    sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }else {
                break;
            }
        }





        //3. 填写成员信息

        //    姓名：    id="username"
        WebElement unameEle = webDriver.findElement(By.id("username"));
        unameEle.clear();
        unameEle.sendKeys("测试人002");
        //    账号：    id="memberAdd_acctid"
        WebElement acctidEle = webDriver.findElement(By.id("memberAdd_acctid"));
        acctidEle.clear();
        acctidEle.sendKeys("20221030223312");
        //    邮箱：    name="biz_mail"
        WebElement bizMailEle = webDriver.findElement(By.name("biz_mail1"));
        bizMailEle.clear();
        bizMailEle.sendKeys("202210301gaigai002");
        //    手机号：    name="mobile"
        WebElement mobileEle = webDriver.findElement(By.xpath("//*[@name=\"mobile\"]"));
        mobileEle.clear();
        mobileEle.sendKeys("13912765433");

        //4. 点击添加
        // a标签   保存
        webDriver.findElement(By.linkText("保存")).click();

        //强制等待
        try {
            sleep(10000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        /**
         * 显示等待
         * By.className("member_colRight_memberTable_td")
         * ..........
         */

        //断言
        //member_colRight_memberTable_td   27
        List<WebElement> tdEleList = webDriver.findElements(By.className("member_colRight_memberTable_td"));
        List<String> tdTitleList = new ArrayList<>();
        tdEleList.forEach(
                td -> {
                    String title = td.getAttribute("title");
                    System.out.println("title：" + title);
                    tdTitleList.add(title);

                }
        );

        System.out.println(tdTitleList);
        assertThat("成员添加失败",tdTitleList.contains("测试人002"));



    }
}
