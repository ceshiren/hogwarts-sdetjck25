/*
 * @Author: 霍格沃兹测试开发学社-盖盖
 * @Desc: '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
 */
package com.ceshiren.basic;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assumptions.assumeThat;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class BaseTest {
    public static WebDriver webDriver;
    //yaml文件解析对象
    static List<HashMap<String,Object>> cookies = null;

    public static WebDriverWait wait;

    @BeforeAll
    public static void bf(){
        //判断当前电脑是否有Chrome浏览器
        Optional<Path> browserPath = WebDriverManager.chromedriver().getBrowserPath();
        assumeThat(browserPath).isPresent();

        //WebDriverManager去创建driver打开浏览器：打开浏览器为空白页面
        webDriver = WebDriverManager.chromedriver().create();
//        webDriver = new ChromeDriver();

        //企业微信登录地址
        webDriver.get("https://work.weixin.qq.com/wework_admin/loginpage_wx");


        wait = new WebDriverWait(webDriver, Duration.ofSeconds(15), Duration.ofSeconds(2));



        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        TypeReference<List<HashMap<String,Object>>> typeReference = new TypeReference<List<HashMap<String,Object>>>(){};

        try {
            cookies = mapper.readValue(Paths.get("cookie.yaml").toFile(), typeReference);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }


        cookies.stream()
                .filter(cookie ->
                        StringUtils.contains(cookie.get("domain").toString(),
                                ".work.weixin.qq.com"))

                //只要是".work.weixin.qq.com" 说明是企业微信的cookie
                .forEach(cookie -> {
                    //cookie放入浏览器操作
                    Cookie cookie1 = new Cookie(
                            cookie.get("name").toString(),
                            cookie.get("value").toString(),
                            cookie.get("path").toString()
                    );
                    webDriver.manage().addCookie(cookie1);
                });
        //刷新浏览器
        webDriver.navigate().refresh();
    }


    @AfterAll
    public static void af(){
        webDriver.quit();
    }
}
