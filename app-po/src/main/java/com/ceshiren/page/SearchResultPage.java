/*
 * @Author: 霍格沃兹测试开发学社-盖盖
 * @Desc: '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
 */
package com.ceshiren.page;

import io.appium.java_client.AppiumBy;
import io.appium.java_client.android.AndroidDriver;

//搜索结果页
public class SearchResultPage extends AppBasePage{


    public SearchResultPage(AndroidDriver androidDriver) {

        super(androidDriver);
    }

    //跳转到个人信息页面
    public ProfilePage toProfilePage(){
        click(
                AppiumBy.xpath(
                        "//*[@text=\"联系人\"]/../following-sibling::*//*[@class=\"android.widget.TextView\"]"));
        waitUtil().until(webDriver -> webDriver.getPageSource().contains("设置备注和描述"));
        return new ProfilePage(androidDriver);
    }

    //跳转到搜索页面
    public SearchPage toSearchPage(){
        click(AppiumBy.className("android.widget.ImageView"));
        waitUtil().until(webDriver -> webDriver.findElement(AppiumBy.xpath(
                "//*[@class=\"android.widget.EditText\" and @text=\"搜索\"]")));

        return new SearchPage(androidDriver);
    }
}
