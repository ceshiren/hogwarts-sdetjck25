/*
 * @Author: 霍格沃兹测试开发学社-盖盖
 * @Desc: '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
 */
package com.ceshiren.page;

import com.ceshiren.entity.AddMember;
import io.appium.java_client.AppiumBy;
import io.appium.java_client.android.AndroidDriver;

//个人信息页面
public class ProfilePage extends AppBasePage{
    public ProfilePage(AndroidDriver androidDriver) {
        super(androidDriver);
    }

    //获取当前用户名，手机号存入User实体类
    public AddMember getMember(){
        String searchName = text(AppiumBy.xpath("//*[@class=\"android.widget.ImageView\"]/following-sibling::*//*[@class=\"android.widget.TextView\"]"));
        //13、获取手机号   getText()
        String searchPhone = text(AppiumBy.xpath("//*[@text=\"手机\"]/following-sibling::*//*[@class=\"android.widget.TextView\"]"));
        user.setName(searchName);
        user.setPhone(searchPhone);
        System.out.println("User:"+user);
        return user;
    }
    // 返回到搜索结果页面
    public SearchResultPage toSearchResultPage(){
        back();//返回搜索结果页面
        //显示等待
        waitUtil().until(webDriver -> webDriver.getPageSource().contains("联系人"));

        return new SearchResultPage(androidDriver);
    }
}
