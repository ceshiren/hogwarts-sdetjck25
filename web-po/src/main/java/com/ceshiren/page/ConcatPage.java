/*
 * @Author: 霍格沃兹测试开发学社-盖盖
 * @Desc: '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
 */
package com.ceshiren.page;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import javax.lang.model.element.Element;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Thread.sleep;

//通讯录页面
public class ConcatPage extends BasePage{
    public ConcatPage(WebDriver driver) {
        super(driver);
    }        
    private By addMember = By.cssSelector(".js_has_member .js_add_member");//查找到2个元素


    //点击添加成员按钮，跳转到添加成员页面
    public MemberPage clickAddMember(){
        System.out.println("进入了通讯录页面，在通讯录页面进行点击添加成员操作，页面跳转");
        //2. 点击添加成员按钮

        //list集合里面的下标为0的元素
        WebElement addMemberEle = find(addMember);
        for (int i = 0; i < 3; i++) {
            if(!driver.getPageSource().contains("username")){
                addMemberEle.click();
                try {
                    sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }else {
                break;
            }
        }
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(15), Duration.ofSeconds(2));
        wait.until(webDriver -> webDriver.getPageSource().contains("企业邮箱："));
        return new MemberPage(driver);
    }


    //获取当前通讯录的成员文本的列表
    public List<String> getMemberListText(){
        //member_colRight_memberTable_td   27
        List<WebElement> tdEleList = driver.findElements(By.className("member_colRight_memberTable_td"));
        List<String> tdTitleList = new ArrayList<>();
        tdEleList.forEach(
                td -> {
                    String title = td.getAttribute("title");
                    System.out.println("title：" + title);
                    tdTitleList.add(title);

                }
        );
        return tdTitleList;

    }

}
