/*
 * @Author: 霍格沃兹测试开发学社-盖盖
 * @Desc: '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
 */
package com.ceshiren.page;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import io.qameta.allure.Step;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

//基本类
//driver 声明
//元素底层的操作
public class BasePage {
    public WebDriver driver;
    static List<HashMap<String,Object>> cookies = null;
    public static WebDriverWait wait;

    public BasePage(WebDriver driver) {
        this.driver = driver;
    }
    public BasePage() {
        if(null == driver){
            //cookie文件是否删除的判断
            //如果cookie文件存在
            cookieFileisDelete();
            driver = new ChromeDriver();
//            driver = WebDriverManager.chromedriver().create();//调用Chrome浏览器
            //隐式等待
            driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
            //显示等待声明
            wait = new WebDriverWait(driver, Duration.ofSeconds(15), Duration.ofSeconds(2));

            //cookie登录
            cookieLogin();
        }
        driver.manage().window().maximize();//窗口最大化
    }

    private static void cookieFileisDelete() {
        if(Paths.get("cookie.yaml").toFile().exists()){
            ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
            TypeReference<List<HashMap<String,Object>>> typeReference = new TypeReference<List<HashMap<String,Object>>>(){};

            try {
                cookies = mapper.readValue(Paths.get("cookie.yaml").toFile(), typeReference);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            //读取出来的cookie对象获取 expiry 不为空的list集合
            List<Long> expiryList = new ArrayList<>();

            cookies.forEach(
                    cookie -> {

                        if(null != cookie.get("expiry")){
                            expiryList.add(Long.parseLong(cookie.get("expiry").toString()));
                        }
                    }
            );
            System.out.println("expiryList:" + expiryList);//[1699260234000,1670316244000]
//            Long getCookieTime = expiryList.get(0) - 一年的毫秒级的数值;
            //     7200s
            long now = System.currentTimeMillis();//当前的毫秒级别
//            if( (now - getCookieTime)/1000  > 7200){
//                //cookie文件删除操作
//            }


        }
    }

    private void cookieLogin() {
        driver.get("https://work.weixin.qq.com/wework_admin/loginpage_wx");


        if(!Paths.get("cookie.yaml").toFile().exists()){//如果当前项目下cookie文件不存在----扫码登录
            //cookie文件写入
            wait.until(webDriver1 -> StringUtils.contains(webDriver1.getCurrentUrl(),"wework_admin/frame"));
            //获取登录成功的cookie，存入yaml文件
            Set<Cookie> cookies = driver.manage().getCookies();

            //yaml文件保存
            ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
            //new File(路径)     -------->     Paths.get(路径).toFile()
//        mapper.writeValue(new File("cookie.yaml"), cookies);
            try {
                mapper.writeValue(Paths.get("cookie.yaml").toFile(), cookies);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }


        //cookie文件读取
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        TypeReference<List<HashMap<String,Object>>> typeReference = new TypeReference<List<HashMap<String,Object>>>(){};

        try {
            cookies = mapper.readValue(Paths.get("cookie.yaml").toFile(), typeReference);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        cookies.stream()
                .filter(cookie ->
                        StringUtils.contains(cookie.get("domain").toString(),
                                ".work.weixin.qq.com"))

                //只要是".work.weixin.qq.com" 说明是企业微信的cookie
                .forEach(cookie -> {
                    //cookie放入浏览器操作
                    Cookie cookie1 = new Cookie(
                            cookie.get("name").toString(),
                            cookie.get("value").toString(),
                            cookie.get("path").toString()
                    );
                    driver.manage().addCookie(cookie1);
                });
        //刷新浏览器   --- 跳转到首页
        driver.navigate().refresh();
    }


    @Step("元素查找:{by}")
    public WebElement find(By by){
        WebElement element = driver.findElement(by);
        HighElement(element);
        //截图
        screen();
        //元素高亮去除
        UnHighElement(element);
        return element;
    }
    public void quite(){
        driver.quit();//浏览器退出操作
    }
    //click
    //send  --- clear、sendKeys
    //getText -- element.getText()
    //finds  -- List<Elements>


    //截图
    private void screen(){
        long now = System.currentTimeMillis();
        File screenshotAs = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        File screenJpg = Paths.get("jpg", now + ".jpg").toFile();
        try {
            FileUtils.copyFile(screenshotAs,screenJpg);
            //allure报告添加截图  --  allure添加附件

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    //js 元素高亮

    private WebDriver HighElement(WebElement webElement){
        if(driver instanceof JavascriptExecutor)
            ((JavascriptExecutor)driver).executeScript("arguments[0].style.border='3px solid red'",webElement);
        return driver;

    }
    private WebDriver UnHighElement(WebElement webElement){
        if(driver instanceof JavascriptExecutor)
            ((JavascriptExecutor)driver).executeScript("arguments[0].style.border=''",webElement);
        return driver;

    }








}
