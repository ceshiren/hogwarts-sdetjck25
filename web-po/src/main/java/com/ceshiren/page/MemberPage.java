/*
 * @Author: 霍格沃兹测试开发学社-盖盖
 * @Desc: '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
 */
package com.ceshiren.page;

import com.ceshiren.page.entity.User;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

//成员相关页面
//添加成员
//编辑成员
//查看成员
public class MemberPage extends BasePage{

    public MemberPage(WebDriver driver) {
        super(driver);
    }

    private By uname = By.id("username");//姓名
    private By acctid = By.id("memberAdd_acctid");//账号
    private By mail = By.name("biz_mail");//邮箱
    private By mail3 = By.name("biz_mail1");//邮箱

    private By mobile = By.xpath("//*[@name=\"mobile\"]");//手机号
    private By save = By.linkText("保存");//保存按钮

    //元素定位私有化
//    private By nameEle = By.id();
    //addMember 添加成员
    //第一种：首页点击跳转到添加成员方法
    //第二种：从通讯录页面点击跳转到添加成员方法
    //正确的添加成员
    public ConcatPage addMemberTrue(User user){
        System.out.println("添加成员，成员的正确添加");
        addMemberStep(user);

        //页面跳转，显示等待跳转条件

        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(15), Duration.ofSeconds(2));
        wait.until(webDriver -> !webDriver.getPageSource().contains("企业邮箱："));
        return new ConcatPage(driver);
    }
    //错误的添加成员 返回值为String，收集的错误信息
    public String addMemberFalse(){
//        addMemberStep();
        return "error";
    }
    private void addMemberStep(User user) {
        //    姓名：    id="username"
        WebElement unameEle = find(uname);
        unameEle.clear();
        unameEle.sendKeys(user.getUname());
        //    账号：    id="memberAdd_acctid"
        WebElement acctidEle = find(acctid);
        acctidEle.clear();
        acctidEle.sendKeys(user.getAcctid());
        //    邮箱：    name="biz_mail"
        WebElement bizMailEle = find(mail);
        bizMailEle.clear();
        bizMailEle.sendKeys(user.getMail());
        //    手机号：    name="mobile"
        WebElement mobileEle = find(mobile);
        mobileEle.clear();
        mobileEle.sendKeys(user.getMobile());

        //4. 点击添加
        // a标签   保存
        find(save).click();
    }

}
