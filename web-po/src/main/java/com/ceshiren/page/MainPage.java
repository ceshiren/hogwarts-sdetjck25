/*
 * @Author: 霍格沃兹测试开发学社-盖盖
 * @Desc: '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
 */
package com.ceshiren.page;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

//企业微信登录成功后的首页
public class MainPage extends BasePage{
    public MainPage() {
    }

    //成员变量


    private By menuContacts = By.id("menu_contacts");
    private By searchConcat = By.id("memberSearchInput");

    /**
     * 无参数构造进行driver 数据声明
     * 有参构造进行driver传递
     */


    //点击通讯录的tab,跳转到通讯录
    public ConcatPage toConcatPage(){
        System.out.println("跳转到通讯录页面");
        //02- 查找元素 返回元素对象
        WebElement menuContactsEle = find(menuContacts);
        //03- 元素操作 点击
        menuContactsEle.click();

        //跳转到通讯录页面成功的显示等待条件为搜索输入框 id="memberSearchInput"
        wait.until(
                webDriver -> webDriver.findElement(searchConcat)
        );
        return new ConcatPage(driver);
    }
    //首页直接点击添加成员按钮跳转到添加成员页面
    public MemberPage toMemberPage(){
        System.out.println("跳转到添加成员页面");
        return new MemberPage(driver);
    }
}
