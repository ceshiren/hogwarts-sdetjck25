package com.ceshiren.page;

import com.ceshiren.page.entity.User;
import com.ceshiren.util.FakerUtil;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;

public class MemberTest {
    static ConcatPage concatPage;
    static MainPage mainPage;
    @Test
    public void addMemberTrue() {
        User user = new User(FakerUtil.get_name(),FakerUtil.get_acctid(),FakerUtil.get_acctid(),FakerUtil.get_zh_phone());
        mainPage = new MainPage();
        concatPage = new MainPage()//首页
                .toConcatPage() //方法//跳转到通讯录页面
                .clickAddMember()//点击添加成员方法//跳转到 添加成员页面
                .addMemberTrue(user);
        List<String> memberTextList = concatPage//正确的添加成员方法//返回到通讯录页面
                                        .getMemberListText();//获取当前成员列表文本方法;
        //断言
        assertThat("成员添加失败",memberTextList.contains(user.getUname()));
    }

    @AfterAll
    public static void af(){
        concatPage.quite();//浏览器的退出操作
//        mainPage.quite();
    }
    @Test
    public void addMemberFalse() {
//        new ConcatPage().clickAddMember()
    }
}