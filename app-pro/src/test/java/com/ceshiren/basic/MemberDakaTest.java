/*
 * @Author: 霍格沃兹测试开发学社-盖盖
 * @Desc: '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
 */
package com.ceshiren.basic;

import com.github.javafaker.App;
import io.appium.java_client.AppiumBy;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.options.app.SupportsAutoGrantPermissionsOption;
import io.appium.java_client.ios.options.wda.SupportsWaitForIdleTimeoutOption;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.PointerInput;
import org.openqa.selenium.interactions.Sequence;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.util.Arrays;

import static java.lang.Thread.sleep;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertAll;

//上下班打卡
public class MemberDakaTest {
    @Test
    public void daka() throws MalformedURLException, InterruptedException {
        DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
        //平台名称 安卓系统就是Android 苹果手机就是iOS platformName
        desiredCapabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
        //使用的driver uiautomator2 automationName
        desiredCapabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, "UiAutomator2");
        //设备的系统版本 安卓手机的系统版本，非小米、华为系统版本号  adb shell getprop ro.build.version.release
        desiredCapabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "12.0.0");
        //启动的app的包名 第三方app：adb shell pm list packages -3    mm wework
        desiredCapabilities.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "com.tencent.wework");
        //启动的app的页面  adb shell monkey -p com.tencent.wework -vvv 1
        /**
         * // Allowing start of Intent {
         * act=android.intent.action.MAIN
         * cat=[android.intent.category.LAUNCHER]
         * cmp=com.tencent.wework/.launch.LaunchSplashActivity } in package com.tencent.wework
         */
        desiredCapabilities.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, ".launch.LaunchSplashActivity");
        //设备名称
        desiredCapabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "8c5f5f92");
        //设备的UDID；adb devices -l 获取，多设备的时候要指定，若不指定默认选择列表的第一个设备
        desiredCapabilities.setCapability(MobileCapabilityType.UDID, "8c5f5f92");
        //app不重置
        desiredCapabilities.setCapability(MobileCapabilityType.NO_RESET, true);
        //运行失败的时候打印page source到appium-log   printPageSourceOnFindFailure
        desiredCapabilities.setCapability(MobileCapabilityType.PRINT_PAGE_SOURCE_ON_FIND_FAILURE, true);
        //在假设客户端退出并结束会话之前，Appium 将等待来自客户端的新命令多长时间（以秒为单位） http请求等待响应最长5分钟  newCommandTimeout
        desiredCapabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 300000);
        //默认权限通过  autoGrantPermissions
        desiredCapabilities.setCapability(SupportsAutoGrantPermissionsOption.AUTO_GRANT_PERMISSIONS_OPTION, true);
        desiredCapabilities.setCapability(SupportsWaitForIdleTimeoutOption.WAIT_FOR_IDLE_TIMEOUT_OPTION, 0);

//------------------------------------------------------------  用例步骤    ------------------------------------------------------------
        URL url = new URL("http://0.0.0.0:4723/wd/hub");
        //1、打开app操作
        AndroidDriver driver = new AndroidDriver(url, desiredCapabilities);
        //添加隐式等待  15秒
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
        //2、点击工作台

        driver.findElement(AppiumBy.xpath("//*[@text=\"工作台\"]")).click();
        sleep(2000);
        //3、滑动  w3c

        Dimension dimension = driver.manage().window().getSize();
        //0.5 x   0.7 y
        Point startPoint = new Point((int) (dimension.width * 0.5), (int) (dimension.height * 0.7));

        Point endPoint = new Point((int) (dimension.width * 0.5), (int) (dimension.height * 0.3));

        PointerInput finger = new PointerInput(PointerInput.Kind.TOUCH, "finger1");
        Sequence dragNDrop = new Sequence(finger, 1)
                                //手指移动到起始坐标点
                                .addAction(finger.createPointerMove(Duration.ofMillis(0),
                                            PointerInput.Origin.viewport(), startPoint.x, startPoint.y))
                                //手指按下
                                .addAction(finger.createPointerDown(0))
                                //滑动到第二个坐标点 滑动时间是1秒
                                .addAction(finger.createPointerMove(Duration.ofMillis(1000),
                                        PointerInput.Origin.viewport(),endPoint.x, endPoint.y))
                                //手指释放
                                .addAction(finger.createPointerUp(0));
        driver.perform(Arrays.asList(dragNDrop));





        //4、点击打卡
        driver.findElement(AppiumBy.xpath("//*[@text=\"打卡\"]")).click();

        //5、显示等待动态加载   你已在打卡范围内

        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(20), Duration.ofMillis(500));
        wait.until(webDriver -> webDriver.getPageSource().contains("你已在打卡范围内"));



        //6、点击上班打卡/下班打卡   contains 班打卡
        driver.findElements(AppiumBy.xpath("//*[contains(@text,\"班打卡\")]")).get(1).click();
        sleep(2000);

        //7、获取pagesource 正常    获取元素包含  正常
        WebElement element = driver.findElement(AppiumBy.xpath("//android.view.View/following-sibling::*//*[@class=\"android.widget.TextView\"]"));
        String text = element.getText();
        String pageSource = driver.getPageSource();

        assertAll(
                ()-> assertThat(text,is(containsString("班·正常"))),
                ()-> assertThat(pageSource,is(containsString("班·正常")))

        );



    }
}
