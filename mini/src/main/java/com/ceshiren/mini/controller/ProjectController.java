//package com.ceshiren.mini.controller;
//
//import com.ceshiren.mini.dto.TestCaseDTO;
//import com.ceshiren.mini.dto.TestTaskAddTDO;
//import com.ceshiren.mini.dto.TestTaskDTO;
//import com.ceshiren.mini.dto.TestTaskRunDTO;
//import com.ceshiren.mini.entity.TestTask;
//import com.ceshiren.mini.service.ProjectService;
//import com.ceshiren.mini.util.R;
//import io.swagger.annotations.ApiOperation;
//import org.springframework.beans.BeanUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.util.StringUtils;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.List;
//import java.util.Objects;
//
//import static com.ceshiren.mini.util.ResultCode.PARAMETER_ERROR;
//import static com.ceshiren.mini.util.ResultCode.PARAMETER_NOT_EXIST;
//
//@RestController
////@RequestMapping("project")
//public class ProjectController {
//
//
//    private ProjectService projectService;
//
//    @Autowired
//    public void setProjectService(ProjectService projectService) {
//        this.projectService = projectService;
//    }
//
//    //创建测试用例
//    @ApiOperation(value = "测试用例的新增")
//    @PostMapping("/case")
//    public R createTestCase(@RequestBody TestCaseDTO testCaseDTO){
//        System.out.println(testCaseDTO);
//        //参数是否为空进行判断
//        //testCaseDTO 不能为空
//        if(Objects.isNull(testCaseDTO)){
//            return R.error(PARAMETER_ERROR).message("测试用例为空");
//        }
//        //caseName  不能为空
//        if(!StringUtils.hasText(testCaseDTO.getCaseName())){
//            return R.error(PARAMETER_ERROR).message("测试用例名称不能为空");
//        }
//        //caseData  不能为空
//        if(!StringUtils.hasText(testCaseDTO.getCaseData())){
//            return R.error(PARAMETER_ERROR).message("测试用例数据不能为空");
//        }
//        //传入参数正常，可以添加测试用例
////        service.createTestCase
//        int insertTestCaseNum = projectService.createTestCase(testCaseDTO);
//        return R.ok().data(testCaseDTO).message(insertTestCaseNum + "条测试用例添加成功");
//    }
//
//    //根据ID获取具体的用例详情
//    @ApiOperation(value = "根据ID获取具体的用例详情")
//    @GetMapping("/case/data/{caseId}")
//    public R getOneTestCase(@PathVariable Integer caseId){
//        TestCaseDTO testCaseDTO = new TestCaseDTO();
//        testCaseDTO.setId(caseId);
//        TestCaseDTO testCaseDto = projectService.getTestCase(testCaseDTO);
//        //返回的实体类如果是空，null  error
//        if(Objects.isNull(testCaseDto))
//            return R.error(PARAMETER_NOT_EXIST).message("用例不存在");
//        return R.ok().data(testCaseDto);
//    }
//
//    //查看用例列表
//    @ApiOperation(value = "查看用例列表")
//    @GetMapping("/case/list")
//    public R getTestCaseList(){
//        List<TestCaseDTO> testCaseDtoList = projectService.getTestCaseList();
//        //返回的实体类如果是空，null  error
//        if(0 == testCaseDtoList.size() || Objects.isNull(testCaseDtoList))
//            return R.error(PARAMETER_NOT_EXIST).message("没有测试用例");
//        return R.ok().data(testCaseDtoList);
//    }
//
//    //测试任务的创建
//    @ApiOperation(value = "测试任务的创建")
//    @PostMapping("/task")
//    public R createTask(@RequestBody TestTaskAddTDO testTaskAddTDO){
//        //testTaskAddTDO 不能为空
//        if(Objects.isNull(testTaskAddTDO)){
//            return R.error(PARAMETER_ERROR).message("测试任务为空");
//        }
//        //name 测试任务名称 不能为空
//        if(!StringUtils.hasText(testTaskAddTDO.getName())){
//            return R.error(PARAMETER_ERROR).message("测试任务名称不能为空");
//        }
//        //[1,4,5]
//        List<Integer> testCaseListId = testTaskAddTDO.getTestCaseListId();
//        int size = testCaseListId.size();//测试用例的个数
//        if(0 == size || Objects.isNull(testCaseListId)){
//            return R.error(PARAMETER_ERROR).message("用例列表不能为空");
//        }
//        //一个任务关联了几条测试用例的个数
//        int insertTaskNum = projectService.createTask(testTaskAddTDO);
//        if(insertTaskNum > 0){
//            if(insertTaskNum == size)
//                return R.ok().data(testTaskAddTDO).message("当前测试任务的所有测试用例关联成功");
//            else
//                return R.ok().data(testTaskAddTDO)
//                        .message("当前测试任务需要关联" + size +"个测试用例,成功关联了" + insertTaskNum + "个测试用例");
//
//        }
//        return R.error(PARAMETER_ERROR).data(testTaskAddTDO);
//    }
//
//    @ApiOperation(value = "根据ID获取测试任务")
//    @GetMapping("task/data/{taskId}")
//    public R getTestTask(@PathVariable Integer taskId){
//        TestTaskDTO testTaskDTO = new TestTaskDTO();
//        testTaskDTO.setId(taskId);
//        testTaskDTO = projectService.getTestTask(testTaskDTO);
//        //返回的实体类如果是空，null  error
//        if(Objects.isNull(testTaskDTO))
//            return R.error(PARAMETER_NOT_EXIST).message("任务不存在");
//        return R.ok().data(testTaskDTO);
//    }
//
//
//    @GetMapping("task/list")
//    @ApiOperation(value = "获取测试任务列表")
//    public R getTestTaskList(){
//
//        List<TestTaskDTO> testTaskDTOS = projectService.getTestTaskList();
//        //返回的实体类如果是空，null  error
//        if(0 == testTaskDTOS.size() ||Objects.isNull(testTaskDTOS))
//            return R.error(PARAMETER_NOT_EXIST).message("任务不存在");
//        return R.ok().data(testTaskDTOS);
//    }
//
//    //任务执行
//    @PostMapping("run")
//    @ApiOperation(value = "执行测试任务")
//    public R runTask(@RequestBody TestTaskRunDTO testTaskRunDTO) {
//
//        //testTaskRunDTO 不能为空
//        if(Objects.isNull(testTaskRunDTO)){
//            return R.error(PARAMETER_ERROR).message("测试任务为空");
//        }
//        //id 测试任务id 不能为空
//        if(Objects.isNull(testTaskRunDTO.getId())){
//            return R.error(PARAMETER_ERROR).message("测试任务ID不能为空");
//        }
//        TestTaskDTO testTaskDTO = new TestTaskDTO();
//        BeanUtils.copyProperties(testTaskRunDTO,testTaskDTO);
//        TestTaskDTO taskDTO = projectService.runTask(testTaskDTO);
//
//        return R.ok().data(taskDTO);
//    }
//}
