package com.ceshiren.mini.dao;

import com.ceshiren.mini.common.MySqlExtensionMapper;
import com.ceshiren.mini.entity.TestTaskCaseRel;
import org.springframework.stereotype.Repository;

@Repository
public interface TestTaskCaseRelMapper extends MySqlExtensionMapper<TestTaskCaseRel> {
}