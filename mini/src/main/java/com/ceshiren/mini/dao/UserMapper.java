package com.ceshiren.mini.dao;

import com.ceshiren.mini.common.MySqlExtensionMapper;
import com.ceshiren.mini.entity.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserMapper extends MySqlExtensionMapper<User> {
}