package com.ceshiren.mini.dao;

import com.ceshiren.mini.common.MySqlExtensionMapper;
import com.ceshiren.mini.entity.TestTask;
import org.springframework.stereotype.Repository;

@Repository
public interface TestTaskMapper extends MySqlExtensionMapper<TestTask> {
}