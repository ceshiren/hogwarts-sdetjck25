/*
 * @Author: 霍格沃兹测试开发学社-盖盖
 * @Desc: '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
 */

package com.ceshiren.basic;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

public class FindDepartTest {
    private static HashMap<String, String> getToken() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper(new JsonFactory());
        TypeReference<HashMap<String,String>> typeReference = new TypeReference<HashMap<String, String>>() {
        };

        HashMap<String, String> map = objectMapper.readValue(new File("token.json"), typeReference);
        return map;
    }
    String URL = "https://qyapi.weixin.qq.com/cgi-bin/department/simplelist";
    @Test
    public void findAll() throws IOException {
        given()
                .log().all()
                .queryParams(getToken())
            .when()
                .request("get",URL)
            .then()
                .log().all()
                .body(matchesJsonSchemaInClasspath("depart/depart.json"));


    }
}
