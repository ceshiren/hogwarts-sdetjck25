/*
 * @Author: 霍格沃兹测试开发学社-盖盖
 * @Desc: '更多测试开发技术探讨，请访问：https://ceshiren.com/t/topic/15860'
 */
package com.ceshiren.basic;

import com.ceshiren.util.FakerUtil;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.filter.TokenFilter;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import io.restassured.response.Response;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.HashMap;

import static io.restassured.RestAssured.given;
import static java.lang.invoke.MethodHandles.lookup;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.slf4j.LoggerFactory.getLogger;

//添加部门测试用例
public class AddDepartTest {
    static final Logger logger = getLogger(lookup().lookupClass());
    String CreatDepartURL = "https://qyapi.weixin.qq.com/cgi-bin/department/create";
    //获取token

    @Test
    public void addDpart() throws IOException {

        HashMap<String, String> token = getToken();


        //部门添加的请求
        /**
         * post
         * URL :
         * https://qyapi.weixin.qq.com/cgi-bin/department/create?access_token=
         */
        String depart = "{\n" +
                "   \"name\": \"广州研发中心\",\n" +
                "   \"name_en\": \"RDGZ\",\n" +
                "   \"parentid\": 1,\n" +
                "   \"order\": 99,\n" +
                "   \"id\": 8\n" +
                "}\n" +
                "\n";

        Response response = given()
                .log().all()
//                .filter(TokenFilter)
                .contentType("application/json; charset=UTF-8")
                .queryParams(token)
                .body(depart)
                .when()
                .post(CreatDepartURL)
                .then()
                .log().all()
                .statusCode(200)
                .extract().response();
        /**
         * {
         *    "errcode": 0,
         *    "errmsg": "created",
         *    "id": 2
         * }
         */
        Integer errcode = response.path("errcode");
        String errmsg = response.path("errmsg");
        Integer id = 0==errcode ? response.path("id") : 0;
        logger.info("id:{}",id);

        assertAll(
                () -> assertEquals(0,errcode) ,
                () -> assertEquals("created",errmsg)
        );

    }

    private static HashMap<String, String> getToken() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper(new JsonFactory());
        TypeReference<HashMap<String,String>> typeReference = new TypeReference<HashMap<String, String>>() {
        };

        HashMap<String, String> map = objectMapper.readValue(new File("token.json"), typeReference);
        return map;
    }

    @Test
    public void jsonPathAddDepart() throws IOException {
        HashMap<String, String> token = getToken();
        //添加部门请求
        DocumentContext context = JsonPath.parse(new File("src/test/resources/depart/add.json"));
        context.set("$.name", FakerUtil.getDepartName());
        context.set("$.order", FakerUtil.getNum(10,100));

        String depart = context.jsonString();
        logger.info("替换后的depart：{}",depart);


        Response response = given()
                .log().all()
                .contentType("application/json; charset=UTF-8")
                .queryParams(token)
                .body(depart)

                .when()
//                .request()
                .post(CreatDepartURL)
                .then()
                .log().all()
                .statusCode(200)
                .extract().response();
        /**
         * {
         *    "errcode": 0,
         *    "errmsg": "created",
         *    "id": 2
         * }
         */
        Integer errcode = response.path("errcode");
        String errmsg = response.path("errmsg");
        Integer id = 0==errcode ? response.path("id") : 0;
        logger.info("id:{}",id);

        assertAll(
                () -> assertEquals(0,errcode) ,
                () -> assertEquals("created",errmsg)
        );

    }


    @Test
    @DisplayName("mustache模版替换")
    public void mustacheAddDepart() throws IOException {
        HashMap<String, String> token = getToken();
        //添加部门请求
        HashMap<String, Object> map = new HashMap<>(){{
            put("departName",FakerUtil.getDepartName()+FakerUtil.getNum(10,100));
            put("orderNum",FakerUtil.getNum(10,100));

        }};
        DefaultMustacheFactory mustacheFactory = new DefaultMustacheFactory();
        Mustache mustache = mustacheFactory.compile("depart/add.mustache");
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(stream);
        mustache.execute(outputStreamWriter,map);
        outputStreamWriter.flush();
        outputStreamWriter.close();

        String depart = new String(stream.toByteArray());
        logger.info("替换后的depart：{}",depart);


        Response response = given()
                .log().all()
                .contentType("application/json; charset=UTF-8")
                .queryParams(token)
                .body(depart)
                .when()
                .post(CreatDepartURL)
                .then()
                .log().all()
                .statusCode(200)
                .extract().response();
        /**
         * {
         *    "errcode": 0,
         *    "errmsg": "created",
         *    "id": 2
         * }
         */
        Integer errcode = response.path("errcode");
        String errmsg = response.path("errmsg");
        Integer id = 0==errcode ? response.path("id") : 0;
        logger.info("id:{}",id);

        assertAll(
                () -> assertEquals(0,errcode) ,
                () -> assertEquals("created",errmsg)
        );

    }
}
