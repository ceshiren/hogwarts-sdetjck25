# 企业ID
corpid:wwecb0ab2da18a7c3b


# Secret
corpsecret:6Yg1wwQ_gZIUTXezXfVtnbSfD01619-jo1LpopQT7w0

# REST-Assured
三剑客：
## given
- 作用：准备请求数据
- 设置请求头，content-Type Header
- 添加接口鉴权信息，比如：cookie
- 添加请求参数
  - get请求：queryParam；也可以直接拼接在URL
  - post请求：body
## when
- 作用：执行，发送请求
- get(URL)
- post(URL)
- put(URL)
- delete(URL)
- request("get",URL)
## then
- 作用：返回结果的获取、验证
- body
- statusCode(200):404、502
- path()  : gpath
- header
- cookie
- getStatusCode



## Schema
- type: 字段的类型
  - integer
  - string
  - object
  - array
  - boolean
  - ....
- properties： 当前JSON对象有哪些属性/字段
- required：必须存在的参数，如果有多个必须字段则为array

```
"errmsg": {
      "type": "string",
      "default": "",
      "title": "The errmsg Schema",
      "examples": [
        "ok"
      ]
    }
```
- default 是type类型的默认值，不是JSON的参数值
- examples 里面为JSON的参数值